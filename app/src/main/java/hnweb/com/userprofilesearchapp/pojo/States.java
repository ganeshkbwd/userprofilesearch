package hnweb.com.userprofilesearchapp.pojo;

/**
 * Created by neha on 7/26/2017.
 */

public class States {

    public String id;
    public String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
