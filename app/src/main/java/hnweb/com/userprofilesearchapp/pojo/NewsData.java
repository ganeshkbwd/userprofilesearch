package hnweb.com.userprofilesearchapp.pojo;

import java.io.Serializable;

/**
 * Created by neha on 8/19/2017..
 * Developer :- Ganesh Kulkarni.
 */

public class NewsData implements Serializable{

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_descp() {
        return news_descp;
    }

    public void setNews_descp(String news_descp) {
        this.news_descp = news_descp;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    String nid, news_title, news_descp, dt, path;

}
