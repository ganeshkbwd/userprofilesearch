package hnweb.com.userprofilesearchapp.pojo;

/**
 * Created by neha on 7/31/2017.
 */

public class ViewReview {

    String name;
    String rating;
    String review;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    String dt;


}
