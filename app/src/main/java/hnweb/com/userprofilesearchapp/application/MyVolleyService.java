package hnweb.com.userprofilesearchapp.application;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

/**
 * Created by neha on 6/26/2017.
 */

public class MyVolleyService {

    IResult mResultCallback = null;
    Context mContext;

    public MyVolleyService(IResult resultCallback, Context context) {
        mResultCallback = resultCallback;
        mContext = context;
    }

    public void postDataVolley(final String requestType, String url, final Map<String, String> params, final String request_tag) {

        final ProgressDialog loadingDialog = new ProgressDialog(mContext);
        loadingDialog.setMessage("Please wait");
        loadingDialog.setCancelable(false);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loadingDialog.show();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                Log.e("REgister", response);
                if (mResultCallback != null)
                    mResultCallback.notifySuccess1(requestType, response, request_tag);
//                ToastUlility.show(All.this, response);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                if (mResultCallback != null)
                    mResultCallback.notifyError1(requestType, error, request_tag);
//
//                ToastUlility.show(RegisterActivity.this, "Network Error,please try again");
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.e("PARAMS", params.toString());
                return params;

            }
        };

//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

    public void getDataVolley(final String requestType, String url, final String request_tag) {

        final ProgressDialog loadingDialog = new ProgressDialog(mContext);
        loadingDialog.setMessage("Please wait");
        loadingDialog.setCancelable(false);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        loadingDialog.show();
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                Log.e("REgister", response);
                if (mResultCallback != null)
                    mResultCallback.notifySuccess1(requestType, response, request_tag);
//                ToastUlility.show(All.this, response);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (loadingDialog.isShowing()) {
                    loadingDialog.dismiss();
                }
                if (mResultCallback != null)
                    mResultCallback.notifyError1(requestType, error, request_tag);
//
//                ToastUlility.show(RegisterActivity.this, "Network Error,please try again");
            }

        });

        stringRequest.setShouldCache(false);
//        queue.add(stringRequest);
        MainApplication.getInstance().addToRequestQueue(stringRequest, request_tag);
    }

}
