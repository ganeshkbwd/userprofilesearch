package hnweb.com.userprofilesearchapp.application;

/**
 * Created by neha on 7/26/2017.
 */

public class AppAPI {

    public static String BASE_URL = "http://104.37.185.20/~tech599/tech599.com/johnaks/userprofilesearch/api/";

    public static String GET_STATES = BASE_URL + "apiups_get_state.php";
    public static String GET_CITIES = BASE_URL + "apiups_get_cities.php";

    public static String ADD_USER = BASE_URL + "apiups_add_user.php";

    public static String ALL_USERS = BASE_URL + "apiups_search_profile.php";

    public static String ADD_REVIEW_RATING = BASE_URL + "apiups_add_rat_review.php";

    public static String VIEW_REVIEW = BASE_URL + "apiups_get_rat_review.php";

    public static String GET_NEWS = BASE_URL + "apiups_get_news.php";
}
