package hnweb.com.userprofilesearchapp.application;

import com.android.volley.VolleyError;

/**
 * Created by neha on 6/26/2017.
 */

public interface IResult {

    public void notifySuccess1(String requestType, String response, String request_tag);

    public void notifyError1(String requestType, VolleyError error, String request_tag);
}