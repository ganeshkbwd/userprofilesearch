package hnweb.com.userprofilesearchapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.userprofilesearchapp.MainActivity;
import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.adapter.NewsDisplayAdapter;
import hnweb.com.userprofilesearchapp.application.AppAPI;
import hnweb.com.userprofilesearchapp.application.IResult;
import hnweb.com.userprofilesearchapp.application.MyVolleyService;
import hnweb.com.userprofilesearchapp.pojo.Cities;
import hnweb.com.userprofilesearchapp.pojo.NewsData;
import hnweb.com.userprofilesearchapp.pojo.States;

/**
 * Created by neha on 7/25/2017.
 */

public class SearchFragment extends Fragment {
    Button searchBTN;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    public Spinner stateS, cityS;
    //    public static HintSpinner<String> defaultHintSpinner;
    //compile 'me.srodrigo:androidhintspinner:1.0.0'
    ArrayList<States> statesArrayList = new ArrayList<States>();
    ArrayList<String> statesNameList = new ArrayList<String>();
    ArrayList<Cities> citiesArrayList = new ArrayList<Cities>();
    ArrayList<String> citiesNameList = new ArrayList<String>();
    EditText wardNoET;
    boolean stateB = false, cityB = false;
    RecyclerView newsRV;
    ArrayList<NewsData> newsDataArrayList = new ArrayList<NewsData>();
    ViewPager viewPager;
//    ClickableViewPager viewPager;


    public SearchFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_search, container, false);

//        ((MainActivity) getActivity()).toolTitle.setText("User Profile Search");
        wardNoET = (EditText) myFragmentView.findViewById(R.id.wardNoET);
        stateS = (Spinner) myFragmentView.findViewById(R.id.stateS);
        cityS = (Spinner) myFragmentView.findViewById(R.id.cityS);
        newsRV = (RecyclerView) myFragmentView.findViewById(R.id.newsRV);
        viewPager = (ViewPager) myFragmentView.findViewById(R.id.viewpager);
//        viewPager = (ClickableViewPager) myFragmentView.findViewById(R.id.viewpager);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        newsRV.setLayoutManager(new LinearLayoutManager(getActivity()));

//        cityS.setClickable(false);
//        wardNoET.setFocusable(false);
        searchBTN = (Button) myFragmentView.findViewById(R.id.searchBTN);
        searchBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (!stateB && !cityB) {
//                    Toast.makeText(getActivity(), "Please select state and city.", Toast.LENGTH_SHORT).show();
//                } else if (!stateB) {
//                    Toast.makeText(getActivity(), "Please select state.", Toast.LENGTH_SHORT).show();
//                } else if (!cityB) {
//                    Toast.makeText(getActivity(), "Please select city.", Toast.LENGTH_SHORT).show();
//                } else {
                String state_id = statesArrayList.get(stateS.getSelectedItemPosition()).getId();
                String city_id = citiesArrayList.get(cityS.getSelectedItemPosition()).getCity_id();
                String wardNo = wardNoET.getText().toString().trim();
                if (TextUtils.isEmpty(state_id) && TextUtils.isEmpty(city_id) && TextUtils.isEmpty(wardNo)) {
                    Toast.makeText(getActivity(), "Please select state, city and enter ward no.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(state_id)) {
                    Toast.makeText(getActivity(), "Please select state.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(city_id)) {
                    Toast.makeText(getActivity(), "Please select city.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(wardNo)) {
                    Toast.makeText(getActivity(), "Please enter ward no.", Toast.LENGTH_SHORT).show();
                } else {
                    AllUsersFragment auf = new AllUsersFragment();
                    ((MainActivity) getActivity()).replaceFragmentWithParams(auf, state_id, city_id, wardNo);
                }
//                }


            }
        });

        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());
        doGetNews();
        doGetStates();
        stateS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                try {
                    doGetCities(statesArrayList.get(position).getId());
                } catch (Exception e) {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                try {
                    doGetCities(statesArrayList.get(0).getId());
                } catch (Exception e) {

                }
            }
        });

//        wardNoET.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                    if (!stateB) {
//                        Toast.makeText(getActivity(), "Please select state.", Toast.LENGTH_SHORT).show();
//                    } else if (!cityB) {
//                        Toast.makeText(getActivity(), "Please select city.", Toast.LENGTH_SHORT).show();
//                    }
//                    return true;
//                }
//                return false;
//            }
//        });

        return myFragmentView;
    }


    public void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);

                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getStates")) {
                            statesNameList.clear();
                            statesArrayList.clear();
                            JSONArray jarr = jobj.getJSONArray("state");
                            for (int i = 0; i < jarr.length(); i++) {
                                States states = new States();
                                states.setId(jarr.getJSONObject(i).getString("id"));
                                states.setName(jarr.getJSONObject(i).getString("state"));
                                statesArrayList.add(states);
                                statesNameList.add(jarr.getJSONObject(i).getString("state"));
                            }
                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                                    android.R.layout.simple_spinner_dropdown_item, statesNameList);

                            stateS.setAdapter(dataAdapter);

//                            defaultHintSpinner = new HintSpinner<>(
//                                    stateS,
//                                    // Default layout - You don't need to pass in any layout id, just your hint text and
//                                    // your list data
//                                    new HintAdapter<String>(getActivity(), "Select State", statesNameList),
//                                    new HintSpinner.Callback<String>() {
//                                        @Override
//                                        public void onItemSelected(int position, String itemAtPosition) {
//                                            // Here you handle the on item selected event (this skips the hint selected
//                                            // event)
//                                            stateB = true;
//                                            doGetCities(statesArrayList.get(position).getId());
//                                        }
//                                    });
//                            defaultHintSpinner.init();
                        } else if (request_tag.equalsIgnoreCase("getCities")) {

                            citiesNameList.clear();
                            citiesArrayList.clear();
                            JSONArray jarr = jobj.getJSONArray("cities");
                            for (int i = 0; i < jarr.length(); i++) {
                                Cities states = new Cities();
                                states.setCity_id(jarr.getJSONObject(i).getString("city_id"));
                                states.setCity_name(jarr.getJSONObject(i).getString("city_name"));
                                states.setState_id(jarr.getJSONObject(i).getString("state_id"));
                                citiesArrayList.add(states);
                                citiesNameList.add(jarr.getJSONObject(i).getString("city_name"));
                            }

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                                    android.R.layout.simple_spinner_dropdown_item, citiesNameList);

                            cityS.setAdapter(dataAdapter);

//                            defaultHintSpinner = new HintSpinner<>(
//                                    cityS,
//                                    // Default layout - You don't need to pass in any layout id, just your hint text and
//                                    // your list data
//                                    new HintAdapter<String>(getActivity(), "Select City", citiesNameList),
//                                    new HintSpinner.Callback<String>() {
//                                        @Override
//                                        public void onItemSelected(int position, String itemAtPosition) {
//                                            // Here you handle the on item selected event (this skips the hint selected
//                                            // event)
//                                            cityB = true;
//                                        }
//                                    });
//                            defaultHintSpinner.init();
                        } else if (request_tag.equalsIgnoreCase("doGetNews")) {
                            JSONArray jarr = jobj.getJSONArray("news");
                            newsDataArrayList.clear();
                            for (int i = 0; i < jarr.length(); i++) {

                                NewsData newsData = new NewsData();
                                newsData.setNid(jarr.getJSONObject(i).getString("nid"));
                                newsData.setNews_title(jarr.getJSONObject(i).getString("news_title"));
                                newsData.setNews_descp(jarr.getJSONObject(i).getString("news_descp"));
                                newsData.setDt(jarr.getJSONObject(i).getString("dt"));
                                newsData.setPath(jarr.getJSONObject(i).getString("path"));
                                newsDataArrayList.add(newsData);
                            }
//                            newsRV.setAdapter(new NewsAdapter(getActivity(), newsDataArrayList));
                            viewPager.setAdapter(new NewsDisplayAdapter(getActivity(), newsDataArrayList, "SEARCH"));

                        }
                    }

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(getActivity(), "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }


    public void doGetStates() {
        mVolleyService.getDataVolley("GETCALL", AppAPI.GET_STATES, "getStates");
    }

    public void doGetCities(String id) {
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        mVolleyService.postDataVolley("POSTCALL", AppAPI.GET_CITIES, params, "getCities");
    }

    public void doGetNews() {
        mVolleyService.getDataVolley("GETCALL", AppAPI.GET_NEWS, "doGetNews");
    }
}
