package hnweb.com.userprofilesearchapp.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.userprofilesearchapp.MultipartRequest.MultiPart_Key_Value_Model;
import hnweb.com.userprofilesearchapp.MultipartRequest.MultipartFileUploaderAsync;
import hnweb.com.userprofilesearchapp.MultipartRequest.OnEventListener;
import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.application.AppAPI;
import hnweb.com.userprofilesearchapp.util.CircleTransform;
import hnweb.com.userprofilesearchapp.util.PickerUtils;
import hnweb.com.userprofilesearchapp.util.RealPathUtil;

/**
 * Created by neha on 7/25/2017.
 */

public class AddUserFragment extends Fragment {
    String stateID, cityID, wardNo;
    private static final int REQUEST_CAMERA = 5;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY = 1002;
    private Uri selectedImageUri = null;
    LinearLayout passLL;
    private String realPath = "";
    private final CharSequence[] items = {"Take Photo", "From Gallery"};
    ImageView profileIV;
    ProgressDialog progressDialog;
    ArrayList<MultiPart_Key_Value_Model> mult_list;
    EditText nameTV, emailTV, phoneTV;
    Button submitBTN;
    File destination;

    public AddUserFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_add_user, container, false);

        stateID = getArguments().getString("StateID");
        cityID = getArguments().getString("CityID");
        wardNo = getArguments().getString("WardNo");

//        ((MainActivity) getActivity()).toolTitle.setText("Add User Profile");
        progressDialog = new ProgressDialog(getActivity());

        submitBTN = (Button) myFragmentView.findViewById(R.id.submitBTN);
        profileIV = (ImageView) myFragmentView.findViewById(R.id.profileIV);
        nameTV = (EditText) myFragmentView.findViewById(R.id.nameTV);
        emailTV = (EditText) myFragmentView.findViewById(R.id.emailTV);
        phoneTV = (EditText) myFragmentView.findViewById(R.id.phoneTV);

        profileIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooserDialog();
            }
        });

        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(nameTV.getText().toString().trim()) &&
                        TextUtils.isEmpty(emailTV.getText().toString().trim()) &&
                        TextUtils.isEmpty(phoneTV.getText().toString().trim()) &&
                        TextUtils.isEmpty(realPath)) {
                    nameTV.setError("Please enter name.");
                    emailTV.setError("Please enter email.");
                    phoneTV.setError("Please enter phone no.");
                    Toast.makeText(getActivity(), "Please select profile picture.", Toast.LENGTH_SHORT).show();

                } else if (TextUtils.isEmpty(realPath.trim())) {
                    Toast.makeText(getActivity(), "Please select profile picture.", Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(nameTV.getText().toString().trim())) {
                    nameTV.setError("Please enter name.");
                } else if (TextUtils.isEmpty(emailTV.getText().toString().trim())) {
                    emailTV.setError("Please enter email.");
                } else if (TextUtils.isEmpty(phoneTV.getText().toString().trim())) {
                    phoneTV.setError("Please enter phone no.");
                } else {
                    progressDialog.show();
                    progressDialog.setMessage("Uploading...");
                    progressDialog.setCancelable(false);
                    doEditProfilePicture(realPath);
                }

            }
        });

        return myFragmentView;
    }


    private void openFileChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        takePictureIntent();
//                        initCameraPermission();
                        break;
                    case 1:
                        initGalleryIntent();
                        break;
                    default:
                }
            }
        });
        builder.show();
    }


    private void initGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_GALLERY);
    }


    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
//                System.out.println("REQUEST_CAMERA");
                realPath = cameraPath();
//                Toast.makeText(getActivity(), "PATH:" + realPath, Toast.LENGTH_SHORT).show();
            } else if (requestCode == REQUEST_GALLERY) {
                selectedImageUri = data.getData();
                if (Build.VERSION.SDK_INT < 11) {
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getActivity(), data.getData());
                } else if (Build.VERSION.SDK_INT < 19) {
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(getActivity(), data.getData());
                } else {
                    realPath = RealPathUtil.getRealPathFromURI_API19(getActivity(), data.getData());
                }
                Log.d("REAL PATH", "real path: " + realPath);
            }
            Glide.with(getActivity()).load(new File(realPath)).override(150, 150).transform(new CircleTransform(getActivity())).into(profileIV);

        }

    }

    public void doEditProfilePicture(final String realPath) {

        mult_list = new ArrayList<MultiPart_Key_Value_Model>();

        MultiPart_Key_Value_Model OneObject = new MultiPart_Key_Value_Model();

        Map<String, String> fileParams = new HashMap<>();
        fileParams.put("file", realPath);

        Map<String, String> Stringparams = new HashMap<>();
        Stringparams.put("state_id", stateID);
        Stringparams.put("city_id", cityID);
        Stringparams.put("ward_no", wardNo);
        Stringparams.put("name", nameTV.getText().toString().trim());
        Stringparams.put("email", emailTV.getText().toString().trim());
        Stringparams.put("phone_no", phoneTV.getText().toString().trim());

        String requestURL = AppAPI.ADD_USER;
        OneObject.setUrl(requestURL);
        OneObject.setFileparams(fileParams);
        OneObject.setStringparams(Stringparams);

        MultipartFileUploaderAsync someTask = new MultipartFileUploaderAsync(getActivity().getApplicationContext(), OneObject, new OnEventListener<String>() {

            @Override
            public void onSuccess(String object) {
//                Toast.makeText(getApplicationContext(), "" + object, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jobj = new JSONObject(object);
                    int message_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");
                    if (message_code == 1) {

                        getActivity().onBackPressed();

                        progressDialog.dismiss();

                    } else {
                        progressDialog.dismiss();
                    }

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Exception e) {

            }
        });
        someTask.execute();
        return;

    }


    public void takePictureIntent() {
        String name = PickerUtils.dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
        destination = new File(Environment.getExternalStorageDirectory(), name + ".png");


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));

        //arshad
        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".my.package.name.provider", destination));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public String cameraPath() {
        try {
            FileInputStream in = new FileInputStream(destination);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 10;
            String imagePath = destination.getAbsolutePath();
            Log.i("Path", imagePath);
//            bmp = BitmapFactory.decodeStream(in, null, options);

//            uploadIV.setImageBitmap(bmp);
            return imagePath;

//            picture.setImageBitmap(bmp);
//            imageVideoUpload(imagePath, hunt_id, hunt_item_id, user_id);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }
}
