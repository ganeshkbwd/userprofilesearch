package hnweb.com.userprofilesearchapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.userprofilesearchapp.MainActivity;
import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.adapter.SearchAllUsersAdapter;
import hnweb.com.userprofilesearchapp.application.AppAPI;
import hnweb.com.userprofilesearchapp.application.IResult;
import hnweb.com.userprofilesearchapp.application.MyVolleyService;
import hnweb.com.userprofilesearchapp.pojo.SearchData;

/**
 * Created by neha on 7/25/2017.
 */

public class AllUsersFragment extends Fragment {

    FloatingActionButton searchFAB, addFAB;
    String stateID, cityID, wardNo;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    ArrayList<SearchData> searchDataArrayList = new ArrayList<SearchData>();
    RecyclerView allUsersRV;
    View myFragmentView;
    AutoCompleteTextView autoCompleteTV;
    SearchAllUsersAdapter allUsersAdapter;


    public AllUsersFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_all_users, container, false);

//        ((MainActivity) getActivity()).toolTitle.setText("Users Profile Search");
        stateID = getArguments().getString("StateID");
        cityID = getArguments().getString("CityID");
        wardNo = getArguments().getString("WardNo");

        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());


//        searchFAB = (FloatingActionButton) myFragmentView.findViewById(R.id.searchFAB);
        autoCompleteTV = (AutoCompleteTextView) myFragmentView.findViewById(R.id.autoCompleteTV);
        addFAB = (FloatingActionButton) myFragmentView.findViewById(R.id.addFAB);
        allUsersRV = (RecyclerView) myFragmentView.findViewById(R.id.allUsersRV);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        allUsersRV.setLayoutManager(layoutManager);
        autoCompleteTV.setThreshold(1);
        autoCompleteTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    allUsersAdapter.getFilter().filter(charSequence);
                }catch (Exception e){

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


//        int spanCount = 2; // 3 columns
//        int spacing = 5; // 50px
//        boolean includeEdge = true;
//        allUsersRV.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

//        searchFAB.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                UserSearchFragment usf = new UserSearchFragment();
//                ((MainActivity) getActivity()).replaceFragmentWithParams(usf, stateID, cityID, wardNo);
//            }
//        });

        addFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddUserFragment auf = new AddUserFragment();
                ((MainActivity) getActivity()).replaceFragmentWithParams(auf, stateID, cityID, wardNo);
            }
        });

        doGetAllUsers();

        return myFragmentView;
    }

    public void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);


                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getAllUsers")) {
                            int count = jobj.getInt("search_cnt");
                            searchDataArrayList.clear();
                            JSONArray jarr = jobj.getJSONArray("search_data");
                            for (int i = 0; i < jarr.length(); i++) {
                                SearchData sd = new SearchData();
                                sd.setUser_id(jarr.getJSONObject(i).getString("user_id"));
                                sd.setName(jarr.getJSONObject(i).getString("name"));
                                sd.setEmail(jarr.getJSONObject(i).getString("email"));
                                sd.setPhone_no(jarr.getJSONObject(i).getString("phone_no"));
                                sd.setImg_path(jarr.getJSONObject(i).getString("img_path"));
                                sd.setCreated_dt(jarr.getJSONObject(i).getString("created_dt"));
                                sd.setStatus(jarr.getJSONObject(i).getString("status"));
                                searchDataArrayList.add(sd);
                            }

                            allUsersAdapter = new SearchAllUsersAdapter(getActivity(), searchDataArrayList, autoCompleteTV, stateID, cityID, wardNo);
                            allUsersRV.setAdapter(allUsersAdapter);
                            String mssg = count + " Profiles found.";
//                            Toast.makeText(getActivity(), count + " Profiles found.", Toast.LENGTH_SHORT).show();
                            Snackbar.make(myFragmentView, count + " Profiles found.", Snackbar.LENGTH_LONG).show();

                        }


                    } else {
                        Snackbar.make(myFragmentView, message, Snackbar.LENGTH_LONG).show();
                    }

//                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Snackbar.make(myFragmentView, "Network Error, please try again later.", Snackbar.LENGTH_SHORT).show();
//                Toast.makeText(getActivity(), "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }


    public void doGetAllUsers() {
        Map<String, String> params = new HashMap<>();
        params.put("state_id", stateID);
        params.put("city_id", cityID);
        params.put("ward_no", wardNo);
        mVolleyService.postDataVolley("POSTCALL", AppAPI.ALL_USERS, params, "getAllUsers");
    }

}
