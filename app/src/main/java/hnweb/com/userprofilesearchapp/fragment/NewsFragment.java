package hnweb.com.userprofilesearchapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.adapter.NewsDisplayAdapter;
import hnweb.com.userprofilesearchapp.pojo.NewsData;

/**
 * Created by neha on 8/24/2017..
 * Developer :- Ganesh Kulkarni.
 */

public class NewsFragment extends Fragment {
    ArrayList<NewsData> newsDataArrayList;
    int position;
    ViewPager viewPager;

    public NewsFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_news, container, false);

        newsDataArrayList = (ArrayList<NewsData>) getArguments().getSerializable("LIST");
        position = getArguments().getInt("POS");

        viewPager = (ViewPager) myFragmentView.findViewById(R.id.viewpager);
        viewPager.setAdapter(new NewsDisplayAdapter(getActivity(), newsDataArrayList, "NEWS"));
        viewPager.setCurrentItem(position);

        return myFragmentView;
    }
}
