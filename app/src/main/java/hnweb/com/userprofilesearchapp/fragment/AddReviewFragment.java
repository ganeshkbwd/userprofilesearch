package hnweb.com.userprofilesearchapp.fragment;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.application.AppAPI;
import hnweb.com.userprofilesearchapp.application.IResult;
import hnweb.com.userprofilesearchapp.application.MyVolleyService;

/**
 * Created by neha on 7/25/2017.
 */

public class AddReviewFragment extends Fragment {

    String user_id;
    EditText reviewET;
    RatingBar ratingTV;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    String TAG = "AddReview";
    EditText nameET;
    Button submitBTN;

    public AddReviewFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_add_review, container, false);

//        ((MainActivity)getActivity()).toolTitle.setText("Add Review Profile");
        user_id = getArguments().getString("StateID");
        nameET = (EditText) myFragmentView.findViewById(R.id.nameET);
        reviewET = (EditText) myFragmentView.findViewById(R.id.reviewET);
        ratingTV = (RatingBar) myFragmentView.findViewById(R.id.ratingTV);
        submitBTN = (Button) myFragmentView.findViewById(R.id.submitBTN);
        ratingBarSet(ratingTV);


        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());

        submitBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.e("RATING", String.valueOf(ratingTV.getRating()));
                checkValid();
            }
        });

        return myFragmentView;
    }

    public void checkValid() {
        if (TextUtils.isEmpty(nameET.getText().toString().trim()) && TextUtils.isEmpty(reviewET.getText().toString().trim()) && ratingTV.getRating() == 0.0) {
            nameET.setError("Please enter name.");
            reviewET.setError("Please enter review.");
            Toast.makeText(getActivity(), "Please give rating.", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(nameET.getText().toString().trim())) {
            nameET.setError("Please enter name.");
        } else if (TextUtils.isEmpty(reviewET.getText().toString().trim())) {
            reviewET.setError("Please enter review.");
        } else if (ratingTV.getRating() == 0.0) {
            Toast.makeText(getActivity(), "Please give rating.", Toast.LENGTH_SHORT).show();
        } else {
//            Toast.makeText(getActivity(), "Rating" + ratingTV.getRating(), Toast.LENGTH_SHORT).show();
            doAddReviewRating();
        }

    }

    public void ratingBarSet(RatingBar setRB) {
        LayerDrawable stars = (LayerDrawable) setRB.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
    }

    public void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);


                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("addReviewRating")) {
                            getActivity().onBackPressed();
                        }


                    }

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e("ADDREVIEW", "Volley requester " + requestType);
                Log.e("ADDREVIEW", "Volley JSON post" + "That didn't work!");
                Toast.makeText(getActivity(), "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }


    public void doAddReviewRating() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", user_id);
        params.put("name", nameET.getText().toString().trim());
        params.put("review", reviewET.getText().toString().trim());
        params.put("rating", String.valueOf(ratingTV.getRating()));
        mVolleyService.postDataVolley("POSTCALL", AppAPI.ADD_REVIEW_RATING, params, "addReviewRating");
    }
}
