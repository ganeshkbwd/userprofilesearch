package hnweb.com.userprofilesearchapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.adapter.SearchAllUsersAdapter;
import hnweb.com.userprofilesearchapp.application.AppAPI;
import hnweb.com.userprofilesearchapp.application.IResult;
import hnweb.com.userprofilesearchapp.application.MyVolleyService;
import hnweb.com.userprofilesearchapp.pojo.SearchData;

/**
 * Created by neha on 7/25/2017.
 */

public class UserSearchFragment extends Fragment {

    ArrayList<SearchData> searchDataArrayList = new ArrayList<SearchData>();
    RecyclerView allUsersRV;
    SearchView searchView;
    SearchAllUsersAdapter allUsersAdapter;
    AutoCompleteTextView autoCompleteTV;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    public String TAG = "REGISTER";
    String stateID, cityID, wardNo;


    public UserSearchFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_user_search, container, false);

//        ((MainActivity) getActivity()).toolTitle.setText("Search User Profile");
        stateID = getArguments().getString("StateID");
        cityID = getArguments().getString("CityID");
        wardNo = getArguments().getString("WardNo");
//        searchDataArrayList.clear();
//        searchDataArrayList = (ArrayList<SearchData>) getArguments().getSerializable("LIST");
        allUsersRV = (RecyclerView) myFragmentView.findViewById(R.id.allUsersRV);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
        allUsersRV.setLayoutManager(layoutManager);

        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());

        doGetAllUsers();

        autoCompleteTV = (AutoCompleteTextView) myFragmentView.findViewById(R.id.autoCompleteTV);
        autoCompleteTV.setThreshold(1);
        autoCompleteTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                allUsersAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
//        autoCompleteTV.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//
//                allUsersAdapter.getFilter().filter(newText);
//                return true;
//            }
//        });

        return myFragmentView;
    }

    public void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);


                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getAllUsers")) {
                            int count = jobj.getInt("search_cnt");
                            searchDataArrayList.clear();
                            JSONArray jarr = jobj.getJSONArray("search_data");
                            for (int i = 0; i < jarr.length(); i++) {
                                SearchData sd = new SearchData();
                                sd.setUser_id(jarr.getJSONObject(i).getString("user_id"));
                                sd.setName(jarr.getJSONObject(i).getString("name"));
                                sd.setEmail(jarr.getJSONObject(i).getString("email"));
                                sd.setPhone_no(jarr.getJSONObject(i).getString("phone_no"));
                                sd.setImg_path(jarr.getJSONObject(i).getString("img_path"));
                                sd.setCreated_dt(jarr.getJSONObject(i).getString("created_dt"));
                                sd.setStatus(jarr.getJSONObject(i).getString("status"));
                                searchDataArrayList.add(sd);
                            }
//                            allUsersRV.setAdapter(new AllUsersAdapter(getActivity(), searchDataArrayList));
                            Toast.makeText(getActivity(), count + " Profiles found.", Toast.LENGTH_SHORT).show();

//                            allUsersAdapter = new SearchAllUsersAdapter(getActivity(), searchDataArrayList, autoCompleteTV);
//                            allUsersRV.setAdapter(allUsersAdapter);
                        }


                    }

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e(TAG, "Volley requester " + requestType);
                Log.e(TAG, "Volley JSON post" + "That didn't work!");
                Toast.makeText(getActivity(), "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }


    public void doGetAllUsers() {
        Map<String, String> params = new HashMap<>();
        params.put("state_id", stateID);
        params.put("city_id", cityID);
        params.put("ward_no", wardNo);
        mVolleyService.postDataVolley("POSTCALL", AppAPI.ALL_USERS, params, "getAllUsers");
    }
}
