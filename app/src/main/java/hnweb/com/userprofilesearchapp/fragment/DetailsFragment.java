package hnweb.com.userprofilesearchapp.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.userprofilesearchapp.MainActivity;
import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.adapter.ViewPagerAdapter;
import hnweb.com.userprofilesearchapp.application.IResult;
import hnweb.com.userprofilesearchapp.application.MyVolleyService;
import hnweb.com.userprofilesearchapp.pojo.SearchData;
import hnweb.com.userprofilesearchapp.pojo.ViewReview;

/**
 * Created by neha on 7/25/2017.
 */

public class DetailsFragment extends Fragment {

//    Button addReviewBTN, viewReviewBTN;
//    RecyclerView viewReviewRV;
//    RelativeLayout addReviewLL;

    ArrayList<SearchData> sal;
    TextView nameTV, emailTV, phoneTV;
    int pos;
    ImageView profileIV;

    IResult mResultCallback;
    MyVolleyService mVolleyService;
    String TAG = "AddReview";
    ArrayList<ViewReview> viewReviewArrayList = new ArrayList<ViewReview>();
    public static String user_id;
    private ViewPager mViewPager;
    private ViewPagerAdapter mViewPagerAdapter;
    private TabLayout mTabLayout;
    FloatingActionButton addFAB;
    String stateID, cityID, wardNo;


    public DetailsFragment() {
        super();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mViewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.fragment_details, container, false);
        sal = (ArrayList<SearchData>) getArguments().getSerializable("LIST");
        pos = getArguments().getInt("POS");
        user_id = sal.get(pos).getUser_id();
        stateID = getArguments().getString("StateID");
        cityID = getArguments().getString("CityID");
        wardNo = getArguments().getString("WardNo");
//        ((MainActivity) getActivity()).toolTitle.setText("User Profile Details");
        init(myFragmentView);
        setData();

        return myFragmentView;
    }

    public void init(View myFragmentView) {

        nameTV = (TextView) myFragmentView.findViewById(R.id.nameTV);
        emailTV = (TextView) myFragmentView.findViewById(R.id.emailTV);
        phoneTV = (TextView) myFragmentView.findViewById(R.id.phoneTV);
        profileIV = (ImageView) myFragmentView.findViewById(R.id.profileIV);
        addFAB = (FloatingActionButton) myFragmentView.findViewById(R.id.addFAB);
        addFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddReviewFragment arf = new AddReviewFragment();
                ((MainActivity) getActivity()).replaceFragmentWithParams(arf, sal.get(pos).getUser_id(), "", "");
            }
        });

        mViewPager = (ViewPager) myFragmentView.findViewById(R.id.pager);
//        mViewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
//        mViewPagerAdapter.addFrag(new PositiveReviewFragment(), "RATING");
//        mViewPagerAdapter.addFrag(new NegativeReviewFragment(), "RATING");
        mViewPager.setAdapter(mViewPagerAdapter);

        mTabLayout = (TabLayout) myFragmentView.findViewById(R.id.tab);
        mTabLayout.setupWithViewPager(mViewPager);

        mTabLayout.getTabAt(0).setCustomView(R.layout.cutom_tab);
        TextView tv = (TextView) mTabLayout.findViewById(R.id.tabTV);
        tv.setText("RATING");
        tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.posssitve,0,0,0);

        mTabLayout.getTabAt(1).setCustomView(R.layout.custom_tab1);
        TextView tv1 = (TextView) mTabLayout.findViewById(R.id.tabTV1);
        tv1.setText("RATING");
        tv1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.nagitive,0,0,0);
    }

    public void setData() {
        nameTV.setText(sal.get(pos).getName());
        emailTV.setText(sal.get(pos).getEmail());
        phoneTV.setText(sal.get(pos).getPhone_no());
        Glide.with(getActivity()).load(sal.get(pos).getImg_path()).override(150, 150).into(profileIV);

    }


}
