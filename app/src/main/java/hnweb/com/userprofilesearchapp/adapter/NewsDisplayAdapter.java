package hnweb.com.userprofilesearchapp.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.userprofilesearchapp.MainActivity;
import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.fragment.NewsFragment;
import hnweb.com.userprofilesearchapp.pojo.NewsData;


/**
 * Created by neha on 7/21/2017.
 */

public class NewsDisplayAdapter extends PagerAdapter {
    Activity activity;
    ArrayList<NewsData> newsDataArrayList;
    String home;


    public NewsDisplayAdapter(Activity activity, ArrayList<NewsData> myList, String home) {
        this.activity = activity;
        this.newsDataArrayList = myList;
        this.home = home;

    }

    @Override
    public int getCount() {
        return newsDataArrayList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.news_item_layout, container, false);
        LinearLayout innerCardLL = (LinearLayout) itemView.findViewById(R.id.innerCardLL);
        TextView dateTV = (TextView) itemView.findViewById(R.id.dateTV);
        TextView titleTV = (TextView) itemView.findViewById(R.id.titleTV);
        TextView descTV = (TextView) itemView.findViewById(R.id.descTV);
        ImageView imgIV = (ImageView) itemView.findViewById(R.id.imgIV);

        dateTV.setText(newsDataArrayList.get(position).getDt());
        titleTV.setText(newsDataArrayList.get(position).getNews_title());
        descTV.setText(newsDataArrayList.get(position).getNews_descp());
        Glide.with(activity).load(newsDataArrayList.get(position).getPath()).into(imgIV);

        innerCardLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (home.equalsIgnoreCase("NEWS")) {

                } else {
                    NewsFragment newsFragment = new NewsFragment();
                    ((MainActivity) activity).replaceFragmentWithParams(newsFragment, newsDataArrayList, position);
                }

            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


}
