package hnweb.com.userprofilesearchapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.userprofilesearchapp.MainActivity;
import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.fragment.NewsFragment;
import hnweb.com.userprofilesearchapp.pojo.NewsData;

/**
 * Created by neha on 8/19/2017..
 * Developer :- Ganesh Kulkarni.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    Activity activity;
    ArrayList<NewsData> newsDataArrayList;

    public NewsAdapter(Activity activity, ArrayList<NewsData> newsDataArrayList) {
        this.activity = activity;
        this.newsDataArrayList = newsDataArrayList;
    }

    @Override
    public NewsAdapter.NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(activity).inflate(R.layout.news_item_layout, null);
        NewsViewHolder dlvh = new NewsViewHolder(layoutView);
        return dlvh;
    }

    @Override
    public void onBindViewHolder(NewsAdapter.NewsViewHolder holder, final int position) {

        holder.dateTV.setText(newsDataArrayList.get(position).getDt());
        holder.titleTV.setText(newsDataArrayList.get(position).getNews_title());
        holder.descTV.setText(newsDataArrayList.get(position).getNews_descp());
        Glide.with(activity).load(newsDataArrayList.get(position).getPath()).into(holder.imgIV);

        holder.innerCardLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewsFragment newsFragment = new NewsFragment();
                ((MainActivity) activity).replaceFragmentWithParams(newsFragment, newsDataArrayList, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsDataArrayList.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {

        public TextView dateTV, titleTV, descTV;
        public ImageView imgIV;
        public LinearLayout innerCardLL;

        public NewsViewHolder(View itemView) {
            super(itemView);
            innerCardLL = (LinearLayout) itemView.findViewById(R.id.innerCardLL);
            dateTV = (TextView) itemView.findViewById(R.id.dateTV);
            titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            descTV = (TextView) itemView.findViewById(R.id.descTV);
            imgIV = (ImageView) itemView.findViewById(R.id.imgIV);
        }
    }
}
