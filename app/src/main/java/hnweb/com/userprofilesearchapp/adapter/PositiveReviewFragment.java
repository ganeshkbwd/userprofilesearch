package hnweb.com.userprofilesearchapp.adapter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.application.AppAPI;
import hnweb.com.userprofilesearchapp.application.IResult;
import hnweb.com.userprofilesearchapp.application.MyVolleyService;
import hnweb.com.userprofilesearchapp.fragment.DetailsFragment;
import hnweb.com.userprofilesearchapp.pojo.ViewReview;

/**
 * Created by neha on 8/21/2017..
 * Developer :- Ganesh Kulkarni.
 */

public class PositiveReviewFragment extends Fragment {

    RecyclerView viewReviewRV;
    IResult mResultCallback;
    MyVolleyService mVolleyService;
    String TAG = "AddReview";
    ArrayList<ViewReview> viewReviewArrayList = new ArrayList<ViewReview>();
    boolean visible = false;

    public PositiveReviewFragment() {
        super();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (visible) {
            if (isVisibleToUser) {
                doGetReviewRating();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View myFragmentView = inflater.inflate(R.layout.positive_review_fragment, container, false);
        visible = true;
        initVolleyCallback();
        mVolleyService = new MyVolleyService(mResultCallback, getActivity());
        viewReviewRV = (RecyclerView) myFragmentView.findViewById(R.id.viewReviewRV);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        viewReviewRV.setLayoutManager(mLayoutManager);
        doGetReviewRating();
        return myFragmentView;
    }

    public void initVolleyCallback() {
        mResultCallback = new IResult() {

            @Override
            public void notifySuccess1(String requestType, String response, String request_tag) {
                Log.e(TAG, "Volley requester ERROR " + requestType);
                Log.e(TAG, "Volley JSON post ERROR" + response);


                try {
                    JSONObject jobj = new JSONObject(response);
                    int meg_code = jobj.getInt("message_code");
                    String message = jobj.getString("message");

                    if (meg_code == 1) {

                        if (request_tag.equalsIgnoreCase("getReviewRating")) {
                            viewReviewArrayList.clear();
                            String count = jobj.getString("list_cnt");
                            JSONArray jarr = jobj.getJSONArray("list_data");
                            for (int i = 0; i < jarr.length(); i++) {
                                ViewReview vr = new ViewReview();
                                vr.setName(jarr.getJSONObject(i).getString("name"));
                                vr.setRating(jarr.getJSONObject(i).getString("rating"));
                                vr.setReview(jarr.getJSONObject(i).getString("review"));
                                vr.setDt(jarr.getJSONObject(i).getString("dt"));

                                if (Float.parseFloat(jarr.getJSONObject(i).getString("rating")) > 3.0) {
                                    viewReviewArrayList.add(vr);
                                }

                            }
                            viewReviewRV.setAdapter(new ReviewsAdapter(viewReviewArrayList, getActivity()));

                        } else if (request_tag.equalsIgnoreCase("addReviewRating")) {
                            getActivity().onBackPressed();
                        }


                    } else {

                    }

                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError1(String requestType, VolleyError error, String request_tag) {
                Log.e("ADDREVIEW", "Volley requester " + requestType);
                Log.e("ADDREVIEW", "Volley JSON post" + "That didn't work!");
                Toast.makeText(getActivity(), "Network Error, please try again later.", Toast.LENGTH_SHORT).show();
            }
        };
    }


    public void doGetReviewRating() {
        Map<String, String> params = new HashMap<>();
        params.put("user_id", DetailsFragment.user_id);
        mVolleyService.postDataVolley("POSTCALL", AppAPI.VIEW_REVIEW, params, "getReviewRating");
    }
}
