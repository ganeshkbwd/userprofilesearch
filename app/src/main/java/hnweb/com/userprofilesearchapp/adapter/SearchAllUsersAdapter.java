package hnweb.com.userprofilesearchapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.userprofilesearchapp.MainActivity;
import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.fragment.DetailsFragment;
import hnweb.com.userprofilesearchapp.pojo.SearchData;

/**
 * Created by neha on 7/25/2017.
 */

public class SearchAllUsersAdapter extends RecyclerView.Adapter<SearchAllUsersAdapter.SearchAllUsersViewHolder> implements Filterable {
    Activity activity;
    ArrayList<SearchData> searchDataArrayList;
    ArrayList<SearchData> mFilteredList;
    AutoCompleteTextView autoCompleteTV;
    String stateID;
    String cityID;
    String wardNo;

    public SearchAllUsersAdapter(Activity activity, ArrayList<SearchData> searchDataArrayList, AutoCompleteTextView autoCompleteTV, String stateID, String cityID, String wardNo) {
        this.activity = activity;
        this.searchDataArrayList = searchDataArrayList;
        this.mFilteredList = searchDataArrayList;
        this.autoCompleteTV = autoCompleteTV;
        this.stateID = stateID;
        this.cityID = cityID;
        this.wardNo = wardNo;
    }

    @Override
    public SearchAllUsersAdapter.SearchAllUsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(activity).inflate(R.layout.all_user_item, null);
        SearchAllUsersViewHolder dlvh = new SearchAllUsersViewHolder(layoutView);
        return dlvh;
    }

    @Override
    public void onBindViewHolder(SearchAllUsersAdapter.SearchAllUsersViewHolder holder, final int position) {


        Glide.with(activity).load(mFilteredList.get(position).getImg_path()).into(holder.propicIV);
        holder.nameTV.setText(mFilteredList.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoCompleteTV.setText("");
                DetailsFragment dt = new DetailsFragment();
                ((MainActivity) activity).replaceFragmentWithParams(dt, searchDataArrayList, position, stateID, cityID, wardNo);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {

                    mFilteredList = searchDataArrayList;
                } else {

                    ArrayList<SearchData> filteredList = new ArrayList<SearchData>();

                    for (SearchData androidVersion : searchDataArrayList) {

                        if (androidVersion.getName().toLowerCase().contains(charString) || androidVersion.getName().toUpperCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<SearchData>) filterResults.values;
//                searchDataArrayList = mFilteredList;
                notifyDataSetChanged();
            }
        };
    }

    public class SearchAllUsersViewHolder extends RecyclerView.ViewHolder {
        public ImageView propicIV;
        public TextView nameTV;

        public SearchAllUsersViewHolder(View itemView) {
            super(itemView);

            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            propicIV = (ImageView) itemView.findViewById(R.id.propicIV);
        }
    }
}
