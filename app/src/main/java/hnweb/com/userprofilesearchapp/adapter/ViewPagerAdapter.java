package hnweb.com.userprofilesearchapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private static int TAB_COUNT = 2;
//    private final List<Fragment> mFragmentList = new ArrayList<>();
//    private final List<String> mFragmentTitleList = new ArrayList<>();


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

//    public void addFrag(Fragment fragment, String title) {
//        mFragmentList.add(fragment);
//        mFragmentTitleList.add(title);
//    }


    @Override
    public Fragment getItem(int position) {
//        return mFragmentList.get(position);
        switch (position) {
            case 0:
                return new PositiveReviewFragment();
            case 1:
                return new NegativeReviewFragment();

        }
        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
//        return mFragmentList.size();
    }

//    @Override
//    public CharSequence getPageTitle(int position) {
//        return mFragmentTitleList.get(position);
////        switch (position) {
////            case 0:
////                return "Positive";
////
////            case 1:
////                return "Negative";
////
////        }
////        return super.getPageTitle(position);
//    }
}
