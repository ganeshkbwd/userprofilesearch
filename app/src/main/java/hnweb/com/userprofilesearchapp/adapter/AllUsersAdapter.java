package hnweb.com.userprofilesearchapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import hnweb.com.userprofilesearchapp.MainActivity;
import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.fragment.DetailsFragment;
import hnweb.com.userprofilesearchapp.pojo.SearchData;

/**
 * Created by neha on 7/25/2017.
 */

public class AllUsersAdapter extends RecyclerView.Adapter<AllUsersAdapter.AllUsersViewHolder> {
    Activity activity;
    ArrayList<SearchData> searchDataArrayList;
    String stateID;
    String cityID;
    String wardNo;


    public AllUsersAdapter(Activity activity, ArrayList<SearchData> searchDataArrayList, String stateID, String cityID, String wardNo) {
        this.activity = activity;
        this.searchDataArrayList = searchDataArrayList;
        this.stateID = stateID;
        this.cityID = cityID;
        this.wardNo = wardNo;
    }

    @Override
    public AllUsersAdapter.AllUsersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(activity).inflate(R.layout.all_user_item, null);
        AllUsersViewHolder dlvh = new AllUsersViewHolder(layoutView);
        return dlvh;
    }

    @Override
    public void onBindViewHolder(final AllUsersAdapter.AllUsersViewHolder holder, final int position) {


        Glide.with(activity)
                .load(searchDataArrayList.get(position).getImg_path())
//                .listener(new RequestListener<String, GlideDrawable>() {
//                    @Override
//                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        holder.progressBar.setVisibility(View.GONE);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                        holder.progressBar.setVisibility(View.GONE);
//                        return false;
//                    }
//                })
                .into(holder.propicIV);
        holder.nameTV.setText(searchDataArrayList.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailsFragment dt = new DetailsFragment();
                ((MainActivity) activity).replaceFragmentWithParams(dt, searchDataArrayList, position,stateID,cityID,wardNo);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchDataArrayList.size();
    }


    public class AllUsersViewHolder extends RecyclerView.ViewHolder {
        public ImageView propicIV;
        public TextView nameTV;
        public ProgressBar progressBar;

        public AllUsersViewHolder(View itemView) {
            super(itemView);

            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            propicIV = (ImageView) itemView.findViewById(R.id.propicIV);
            progressBar = (ProgressBar) itemView.findViewById(R.id.myprofile_photo_progress);
        }
    }
}
