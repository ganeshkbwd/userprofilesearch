package hnweb.com.userprofilesearchapp.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import hnweb.com.userprofilesearchapp.R;
import hnweb.com.userprofilesearchapp.pojo.ViewReview;

/**
 * Created by neha on 7/25/2017.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewsViewHolder> {

    ArrayList<ViewReview> viewReviewArrayList;
    Activity activity;

    public ReviewsAdapter(ArrayList<ViewReview> viewReviewArrayList, Activity activity) {
        this.viewReviewArrayList = viewReviewArrayList;
        this.activity = activity;

    }

    @Override
    public ReviewsAdapter.ReviewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(activity).inflate(R.layout.view_review_item, null);
        ReviewsViewHolder dlvh = new ReviewsViewHolder(layoutView);
        return dlvh;
    }

    public void ratingBarSet(RatingBar setRB) {
        LayerDrawable stars = (LayerDrawable) setRB.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public void onBindViewHolder(ReviewsAdapter.ReviewsViewHolder holder, int position) {


//            holder.cardView.setVisibility(View.VISIBLE);
//            holder.cardViewL.setVisibility(View.GONE);
        holder.dateTV.setText(viewReviewArrayList.get(position).getDt());
        holder.nameTV.setText(viewReviewArrayList.get(position).getName());
        holder.reviewTV.setText(viewReviewArrayList.get(position).getReview());
        holder.ratingbar.setRating(Float.parseFloat(viewReviewArrayList.get(position).getRating()));


//        else {
//            holder.cardView.setVisibility(View.GONE);
//            holder.cardViewL.setVisibility(View.VISIBLE);
//            holder.dateTVL.setText(viewReviewArrayList.get(position).getDt());
//            holder.nameTVL.setText(viewReviewArrayList.get(position).getName());
//            holder.reviewTVL.setText(viewReviewArrayList.get(position).getReview());
//            holder.ratingbarL.setRating(Float.parseFloat(viewReviewArrayList.get(position).getRating()));
//        }


    }

    @Override
    public int getItemCount() {
        return viewReviewArrayList.size();
    }

    public class ReviewsViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTV, reviewTV, dateTV;
        public RatingBar ratingbar;
        public CardView cardView;
//        public TextView nameTVL, reviewTVL, dateTVL;
//        public RatingBar ratingbarL;
//        public CardView cardViewL;


        public ReviewsViewHolder(View itemView) {
            super(itemView);

//            cardView = (CardView) itemView.findViewById(R.id.card_view);
            nameTV = (TextView) itemView.findViewById(R.id.nameTV);
            reviewTV = (TextView) itemView.findViewById(R.id.reviewTV);
            ratingbar = (RatingBar) itemView.findViewById(R.id.ratingTV);
            dateTV = (TextView) itemView.findViewById(R.id.dateTV);
            ratingBarSet(ratingbar);
//            cardViewL = (CardView) itemView.findViewById(R.id.card_viewL);
//            nameTVL = (TextView) itemView.findViewById(R.id.nameTVL);
//            reviewTVL = (TextView) itemView.findViewById(R.id.reviewTVL);
//            ratingbarL = (RatingBar) itemView.findViewById(R.id.ratingTVL);
//            dateTVL = (TextView) itemView.findViewById(R.id.dateTVL);
        }
    }
}
