package hnweb.com.userprofilesearchapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import hnweb.com.userprofilesearchapp.fragment.SearchFragment;
import hnweb.com.userprofilesearchapp.pojo.NewsData;
import hnweb.com.userprofilesearchapp.pojo.SearchData;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    ActionBarDrawerToggle drawerToggle;
    DrawerLayout drawerLayout;
    LinearLayout settingDrawerLL;
    public TextView toolTitle;
    public String realPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolBar();

        SearchFragment myFragment = new SearchFragment();
        replaceFragment(myFragment);
    }

    public void replaceFragment(android.support.v4.app.Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragmentTransaction.commit();
    }

    public void replaceFragmentWithParams(android.support.v4.app.Fragment fragment, String StateID, String CityID, String WardNo) {
        Bundle bundle = new Bundle();
        bundle.putString("StateID", StateID);
        bundle.putString("CityID", CityID);
        bundle.putString("WardNo", WardNo);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    public void replaceFragmentWithParams(android.support.v4.app.Fragment fragment, ArrayList<SearchData> sal, int position, String StateID, String CityID, String WardNo) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("LIST", sal);
        bundle.putInt("POS", position);
        bundle.putString("StateID", StateID);
        bundle.putString("CityID", CityID);
        bundle.putString("WardNo", WardNo);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    public void replaceFragmentWithParams(android.support.v4.app.Fragment fragment, ArrayList<NewsData> sal, int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("LIST", sal);
        bundle.putInt("POS", position);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.myFragmentLL, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.addToBackStack(fragment.getClass().getName());
        fragment.setArguments(bundle);
        fragmentTransaction.commit();
    }

    public void toolBar() {
        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
//        toolTitle = (TextView) toolbar.findViewById(R.id.toolTitle);
//        toolTitle.setText("User Profile Search");

        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        settingDrawerLL = (LinearLayout) findViewById(R.id.settings_drawer_layout);


        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
//                    Constants.profileWebservice(MyHunts_Activity.this, String.valueOf(user_id), iv, nameTV);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
